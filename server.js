var gzippo = require('gzippo');
var path = require('path');
var express = require('express');
var bodyParser=require("body-parser");
var app=express();
app.use(gzippo.staticGzip("" + __dirname + "/"));
app.use(gzippo.staticGzip("" + __dirname + "/angular-material-keyboard/gist"));
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/control', function(req, res) {
    res.sendFile(path.join(__dirname + '/control.html'));
});app.get('/delete', function(req, res) {
    res.sendFile(path.join(__dirname + '/delete.html'));
});
app.get('/D87CF93C5C18E211586395B714161', function(req, res) {
    res.sendFile(path.join(__dirname + '/control.html'));
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.all('/*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header('Access-Control-Allow-Headers', 'Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    next();
});
var server = app.listen(process.env.PORT, function() {
    console.log('Express server listening on port ' + server.address().port);
});