Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
var underscore = angular.module('underscore', []);
underscore.factory('_', ['$window', function($window) {
    return $window._; // assumes underscore has already been loaded on the page
}]);
angular.module('Front', ['ngMaterial','material.components.keyboard','underscore','angular-momentjs'])

    .directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .config(function ($mdKeyboardProvider) {

        // add layout for number fields
        $mdKeyboardProvider.addLayout('Numbers', {
            'name': 'Numbers', 'keys': [
                [['7', '7'], ['8', '8'], ['9', '9'], ['Bksp', 'Bksp']],
                [['4', '4'], ['5', '5'], ['6', '6'], ['-', '-']],
                [['1', '1'], ['2', '2'], ['3', '3'], ['+', '+']],
                [['0', '0'], ['Spacer'], [','], ['Enter', 'Enter']]
            ], 'lang': ['en']
        });

        // default layout is german
        $mdKeyboardProvider.defaultLayout('US International');
    })
    .config(function($momentProvider){
        $momentProvider
            .asyncLoading(false)
            .scriptUrl('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js');
    })
    .controller('FrontCtrl', function($scope,_,$moment) {
        $scope.orderValue='';
        $scope.all=[] ;
        $scope.todayString = new moment().format("DD/MM/YYYY");
        $scope.momentToday = new moment();
        $scope.updateWaiting=function(arr) {
            if (arr === null)
                return;

            $scope.all = arr;

            //var filtered = _(arr).pairs().filter(function (item) {
            //    return item.status === 'waiting';
            //});
            //

            var data = _(arr).pairs().filter(_.last);

            /*starting off*/
            if ($scope.waiting.length === 0 && arr.length === 1)
                $scope.waiting.push({order: data[0][1].order, status: 'waiting', timestamp: data[0][1].timestamp,id: data[0][1].id})


            /*more than 1 orders already*/
            var diff;
            if (data.length > 0) {

                //everytime there is an update, two cases, either new order came in or existing got deleted
                //so $scope.waiting will need to be updated accordingly

                _.each(data, function (i) {
                    var current = i[1];
                    var diff;
                    //check if waiting knows of current order
                    diff = _.where($scope.waiting, {id: current.id});
                    if (diff.length === 0 && current.status === 'waiting') //
                        $scope.waiting.push({order: current.order, status: 'waiting', timestamp: current.timestamp, id: current.id});

                    if(current.status==='deleted')
                    {
                        var w= _.filter($scope.waiting,function(x){
                            return x.id!==current.id
                        })

                        $scope.waiting=w;
                    }

                });

                _.defer(function () {
                    $scope.$apply();
                });

            }
        }

        //$scope.updateAll=function(orders){
        //    //$scope.reset();
        //    _.each(orders,function(order){
        //        if(order.status==='ready' && moment().diff(momentDate(order.timestamp),'days')===0)
        //        {
        //            $scope.ready.push({order:order.order,status:'ready',timestamp:$scope.todayString});
        //
        //        }
        //        else if(order.status==='waiting' && moment().diff(momentDate(order.timestamp),'days')===0)
        //
        //        {
        //            $scope.waiting.push({order:order.order,status:'waiting',timestamp:$scope.todayString});
        //        }
        //    })
        //
        //    $scope.waiting=_.uniq($scope.waiting,'order');
        //    $scope.waiting = _( orders).filter(function(item){
        //        return (item.status==='waiting'&& moment().diff(momentDate(item.timestamp),'days')===0);
        //    });
        //    $scope.ready= _.uniq($scope.ready,'order');
        //    $scope.ready = _(orders).filter(function(item){
        //        return (item.status==='ready' && moment().diff(momentDate(item.timestamp),'days')===0);
        //    });
        //    _.defer(function(){
        //        $scope.$apply();
        //    });
        //}
        //$scope.clicked=function(item)
        //{
        //
        //    var toSave=[{
        //        order:item.order,
        //        status:'ready',
        //        timestamp:$scope.todayString
        //}];
        //    saveToList(toSave);
        //    $scope.waiting=_($scope.waiting).filter(function(item) {
        //        return item.order !== toSave.order
        //    });
        //    _.defer(function(){
        //        $scope.$apply();
        //    });
        //}
        $scope.pushOrder=function()
        {
            //var isDupe=false;
            var random = Math.random();
            //_.each($scope.waiting,function(item){
            //    if(item.order===$scope.orderValue && item.status ==='waiting')
            //        isDupe=true;
            //})
            //_.each($scope.all,function(item){
            //    if(item.order===$scope.orderValue && item.status ==='waiting')
            //        isDupe=true;
            //})

            //if(!isDupe)
            //{
            //
            //}

            //$scope.waiting.push({order: $scope.orderValue, status: 'waiting', timestamp: $scope.todayString});
            var toSave = [{
                order: $scope.orderValue,
                status: 'waiting',
                timestamp: $scope.todayString,
                id:random
            }];

            saveToList(toSave);
        }


        $scope.waiting = [


        ]; $scope.ready = [

        ];
        $scope.delete = function(item)
        {

            var key=_.findKey($scope.all, { 'id': item.id});

            orders.child(key).update({"status": 'deleted', "order":item.order,"timestamp":$scope.todayString,"id":item.id });
            var w= _.filter($scope.waiting,function(x){
                return x.id!==item.id
            })

            $scope.waiting=w;
            _.defer(function(){
                $scope.$apply();
            });
        }


    });